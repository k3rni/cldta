(ns cldta.core
  (:require [clojure.string :as str]))


(defn read-bytes [reader len]
  (let [buf (char-array len)
       number-read (.read reader buf)]
    (if (= -1 number-read) nil buf)))

(defn read-str-of [reader len]
    (apply str (map char (read-bytes reader len))))

(defn german-boolean [str]
  (case (first str)
    \J true
    \N false
    ))

(defstruct q-record :bank-code :customer-account
                    :principal-name :principal-address :principal-city
                    :created-at :serial :execution-date
                    :notify-authorities :federal-state-number
                    :principal-bank-code)

(defn read-q-record [reader]
  (let [feed (fn [len] (read-str-of reader len))
        q (struct q-record
                  (feed 8) ; bank-code
                  (feed 10) ; customerAccount
                  (str/trim (feed 70)) ; principal-name
                  (str/trim (feed 35)) ; principal-address
                  (str/trim (feed 35)) ; principal-city
                  (feed 6) ; created-at YYMMDD
                  (feed 2) ; serial
                  (feed 6) ; execution-date
                  (german-boolean (feed 1)) ; notify-authorities
                  (feed 2) ; federal-state-number
                  (feed 8) ; principal-bank-code
                  )]
    (do
      (.skip reader 68) ; padding
      q
      )))

(defstruct t-record :payer-bank-code :payer-currency :payer-account :execution-date
                    :expenses-bank-code :expenses-currency :expenses-account
                    :provider-bic :provider-country :provider-name :provider-address :provider-city
                    :recipient-country :recipient-name :recipient-address :recipient-city
                    :order-mark :recipient-iban :currency :amount ; note amount is two fields in the spec
                    :details :ic1 :ic2 :ic3 :ic4 :additional
                    :fee-rule :payment-type-code :settlement-text :contact :reporting-code :extension-num)

(defn read-t-record [reader]
  (let [feed (fn [len] (read-str-of reader len))
        trim-feed (fn [len] (str/trim (feed len)))
        t (struct t-record
         (feed 8) ; 3 payer-bank-code
         (symbol (feed 3)) ; 4a payer-currency
         (feed 10) ; 4b payer-account
         (feed 6) ; 5 execution-date
         (feed 8) ; 6 expenses-bank-code
         (symbol (feed 3)) ; 7a expenses-currency
         (feed 10) ; 7b expenses-account
         (feed 11) ; 8 provider-bic
         (trim-feed 3) ; 9a provider-country
         (trim-feed 70) ; 9b provider-name
         (trim-feed 35) ; 9b provider-address
         (trim-feed 35) ; 9b provider-city
         (trim-feed 3) ; 10a recipient-country
         (trim-feed 70) ; 10b recipient-name
         (trim-feed 35) ; 10b recipient-address
         (trim-feed 35) ; 10b recipient-city
         (trim-feed 70) ; 11 order mark
         (trim-feed 35) ; 12 recipient-iban
         (symbol (feed 3)) ; 13 currency
         (bigdec (str (feed 14) "." (feed 3))) ; 14a 14b amount
         (trim-feed 140) ; 15 details
         (feed 2) (feed 2) (feed 2) (feed 2) ; 16 17 18 19 ic1-ic4
         (trim-feed 25) ; 20 additional
         (feed 2) ; 21 fee-rule
         (feed 2) ; 22 payment-type-code
         (trim-feed 27) ; 23 settlement-text
         (trim-feed 35) ; 24 contact
         (bigint (feed 1)) ; 25 reporting-code
         (do (.skip reader 51) (feed 2))) ; 26 reserved, 27 extension-num
        ]
    t))

(defstruct z-record :total :transaction-count)

(defn read-z-record [reader]
  (let [feed (fn [len] (read-str-of reader len))
        z (struct z-record
                  (bigint (feed 15))
                  (bigint (feed 15)))]
    (do (.skip reader 221) z)))

(defn read-single-record [reader]
  (let [record-identifier (.read reader)
        record (case (char record-identifier)
          \Q (read-q-record reader)
          \T (read-t-record reader)
          \Z (read-z-record reader))]
    record))

(defn parse-from-reader [reader]
  (loop [acc []] ; TODO: convert to lazy-seq https://stackoverflow.com/a/26422738
    (let [size-marker (read-bytes reader 4)]
      (if (nil? size-marker) ; this is the length declaration, but we don't care about it, other than using it to detect eof
        acc
        (let [record (read-single-record reader)]
          (recur (conj acc record))))
    )))

(defn lazy-parse-from-reader [reader]
  "this doesn't work because reader is let go of (and closed) immediately after consuming the first record :("
  (letfn [(step [input]
             (lazy-seq
               (if-let [size-marker (read-bytes input 4)]
                 (let [record (read-single-record input)]
                   (cons record (step input)))
                 (do (.close input) nil))))]
   (step reader)))

(defn parse-file [filename]
  (with-open [reader (clojure.java.io/reader filename) ]
    (parse-from-reader reader)))

(defn lazy-parse-file [filename]
  (with-open [reader (clojure.java.io/reader filename) ]
    (lazy-parse-from-reader reader)))

